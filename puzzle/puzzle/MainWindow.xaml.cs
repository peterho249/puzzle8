﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.IO;
using Microsoft.Win32;

namespace puzzle
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int[,] puzzleMatrix = new int[3, 3];
        private const int DefaultSize = 100;
        private int newiPos = 0;
        private int newjPos = 0;
        private int startTop = 180;
        private int startLeft = 10;
        private Bitmap[] partImage = new Bitmap[8];
        String baseDir = Directory.GetCurrentDirectory();
        private int mode = 0;

        public MainWindow()
        {
            InitializeComponent();
            InitPuzzleMatrix();
            CropImage(baseDir + @"\jarvis.png");
            LoadPuzzle();
        }

        public int[,] PuzzleMatrix { get => puzzleMatrix; set => puzzleMatrix = value; }

        /// <summary>
        /// Initial 8-puzzle matrix
        /// </summary>
        private void InitPuzzleMatrix()
        {
            // Value template
            var template = new int[9] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

            // Shuffle array
            // Source: https://stackoverflow.com/questions/14535274/randomly-permutation-of-n-consecutive-integer-number
            Random random = new Random();
            int n = template.Count();
            while (n > 1)
            {
                n--;
                int i = random.Next(n + 1);
                int temp = template[i];
                template[i] = template[n];
                template[n] = temp;
            }

            // Convert array to matrix
            for (var i = 0; i < 3; i++)
            {
                for (var j = 0; j < 3; j++)
                {
                    PuzzleMatrix[i, j] = template[i * 3 + j];
                }
            }
        }

        /// <summary>
        /// Check whether win or not
        /// </summary>
        /// <returns>True if win</returns>
        private bool IsWin()
        {
            if (PuzzleMatrix[2, 2] != 0)
                return false;

            int last = 0;

            for (var i = 0; i < 3; i++)
            {
                for (var j = 0; j < 3; j++)
                {
                    if (PuzzleMatrix[i, j] - last != 1 && PuzzleMatrix[i, j] - last != -8)
                        return false;

                    last = PuzzleMatrix[i, j];
                }
            }

            return true;
        }

        /// <summary>
        /// Move the puzzle.
        /// </summary>
        /// <param name="buttonTag">Tag number of puzzle</param>
        /// <returns>True if the puzzle is moved</returns>
        private bool Move(int buttonTag)
        {
            int i = -1;
            int j = -1;

            for (int index = 0; index < 9; index++)
            {
                i = index / 3;
                j = index % 3;
                if (PuzzleMatrix[i, j] == buttonTag)
                    break;
            }

            int i0 = -1;
            int j0 = -1;
            bool foundFlag = false;
            for (int iFactor = -1; iFactor <= 1; iFactor++)
            {
                for (int jFactor = -1; jFactor <= 1; jFactor++)
                {
                    if (iFactor * jFactor != 0)
                        continue;

                    i0 = i + iFactor;
                    j0 = j + jFactor;

                    if (i0 < 0 || i0 > 2 || j0 < 0 || j0 > 2)
                        continue;

                    if (PuzzleMatrix[i0, j0] == 0)
                    {
                        foundFlag = true;
                        break;
                    }
                }

                if (foundFlag)
                    break;
            }

            if (foundFlag)
            {
                PuzzleMatrix[i0, j0] = PuzzleMatrix[i, j];
                PuzzleMatrix[i, j] = 0;
                newiPos = i0;
                newjPos = j0;
            }

            return foundFlag;
        }

        /// <summary>
        /// Load button on canvas
        /// </summary>
        private void LoadPuzzle()
        {
            for (var i = 0; i < 9; i++)
            {
                int iIndex = i / 3;
                int jIndex = i % 3;

                if (PuzzleMatrix[iIndex, jIndex] == 0)
                    continue;

                Button button = new Button
                {
                    Tag = PuzzleMatrix[iIndex, jIndex],
                    Height = DefaultSize,
                    Width = DefaultSize,
                    Content = new System.Windows.Controls.Image
                    {
                        Source = new BitmapImage(new Uri(baseDir + "\\part" + PuzzleMatrix[iIndex, jIndex] + mode + ".bmp")),
                        VerticalAlignment = VerticalAlignment.Center
                    }
                };
                button.Click += Puzzle_Click;

                
                

                //button.Content = partImage[PuzzleMatrix[iIndex, jIndex] - 1];

                Canvas.SetTop(button, startTop + iIndex * DefaultSize);
                Canvas.SetLeft(button, startLeft + jIndex * DefaultSize);
                MainCanvas.Children.Add(button);
            }
        }

        void Puzzle_Click(object sender, RoutedEventArgs e)
        {
            if (Move((int)(sender as Button).Tag))
            {
                Button btn = sender as Button;
                Canvas.SetTop(btn, startTop + newiPos * DefaultSize);
                Canvas.SetLeft(btn, startLeft + newjPos * DefaultSize);
                if (IsWin())
                {
                    MessageBox.Show("Ban da hoan thanh game!!");
                }
            }
        }

        private void RestartButton_Click(object sender, RoutedEventArgs e)
        {
            MainCanvas.Children.Clear();
            InitPuzzleMatrix();
            LoadPuzzle();
        }

        private void CropImage(String path)
        {
            System.Drawing.Image sourceImage = System.Drawing.Image.FromFile(path);
            Bitmap bmpSource = new Bitmap(sourceImage);

            for (var i = 0; i < 8; i++)
            {
                int iIndex = i / 3;
                int jIndex = i % 3;

                System.Drawing.Rectangle cropRect =
                    new System.Drawing.Rectangle(jIndex * DefaultSize, iIndex * DefaultSize, 100, 100);

                partImage[i] = bmpSource.Clone(cropRect, bmpSource.PixelFormat);
                File.Delete(baseDir + "\\part" + (i + 1) + mode + ".bmp");
                partImage[i].Save(baseDir + "\\part" + (i + 1) + mode + ".bmp", System.Drawing.Imaging.ImageFormat.Png);
                partImage[i].Dispose();
            }
        }

        private void OpenImageButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog
            {
                Filter = "Image (*.png)|*.png|All files (*.*)|*.*"
            };

            if (openFile.ShowDialog()==true)
            {
                MainCanvas.Children.Clear();
                mode = mode + 1;
                SampleImage.Source = new BitmapImage(new Uri(openFile.FileName));
                InitPuzzleMatrix();
                CropImage(openFile.FileName);
                LoadPuzzle();
            }
        }
    }
}
