# README #

## Information ##
* ID: 1512077
* Name: Ho Xuan Dung

## Configuration System ##
* OS: Windows 10 x64
* IDE: Visual Studio 2017

## Function ##
* User can click on button to move a puzzle.
* An message box popup when user win the game.
* Generate 8 button from code.
* The image part on button will be automative cropped from source.
* User can choose image source by Open dialog. **(Note: For best performance, image should be a png image in 300x300 pixels)**

## Application Flow ##
### Main Flow ###
* User click on button to move it to new position.
* User click on restart button to restart the game with random position of puzzle.
* User click on Load image button to open new image.

## Note ##
* To run application successfully, please placing a png image in size 300x300 px in the same folder of exe and and name it into jarvis.png. Or you can use the available image in folder Release.

## BitBucket Link ##
* https://bitbucket.org/peterho249/puzzle8

## Youtube Link ##
* https://youtu.be/w1SNIeu-P04